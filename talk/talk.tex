\documentclass[utf8x,smaller]{beamer} 
% incscape produced pictures come from here!
\graphicspath{{svg/}}

% Latex-beamer settings
\usefonttheme[onlymath]{serif}
\usefonttheme{structurebold} % quite nice
\usetheme{Boadilla} % plain white with blue headings
\useinnertheme[shadow]{rounded}
\usecolortheme{rose} % orchid for a more dark version
\beamertemplatebookbibitems
\setbeamertemplate{navigation symbols}{} % seltsame Symbole ausblenden
\definecolor{secinhead}{RGB}{249,196,95}

\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
  \begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}%
    \usebeamerfont{author in head/foot}\insertshortauthor
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
    \usebeamerfont{title in head/foot}\insertsection
  \end{beamercolorbox}%
  \begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%
    \usebeamerfont{date in head/foot}\insertsubsection\hspace*{2em}
    \insertframenumber{} / \inserttotalframenumber\hspace*{2ex} 
  \end{beamercolorbox}}%
  \vskip0pt%
}
\usepackage{hyperref}
\usepackage{pifont}
\usepackage[absolute,overlay]{textpos}
\textblockorigin{1em}{2em} % coordinates of zero/zero
\setlength{\TPHorizModule}{.10\textwidth}   % units are 1/10th of a page width
\setlength{\TPVertModule}{.10\textheight}   % units are 1/10th of a page height
\newcommand{\absolute}[4]{
    \begin{textblock}{0.1}(#1,#2) % {horizontale Ausdehnung}(linksoben_x, linkoben_y) ?
    \pgfimage[height=#3]{#4}
    \end{textblock}
}
\usepackage{graphicx,colortbl,etex,helvet,tikz}

%Source Code highlighting
\usepackage{minted}
\usemintedstyle{colorful}
%Background for minted source code
\definecolor{almostnogray}{rgb}{0.92,0.92,0.92}
\definecolor{dgreen}{rgb}{0.1,0.5,0.1}
\definecolor{dgrey}{rgb}{0.5,0.5,0.5}
\definecolor{dred}{rgb}{0.5,0.1,0.1}

\newcommand{\yes}{\Large\color{dgreen}\ding{51}}
\newcommand{\no}{\Large\color{dred}\ding{55}}
\newcommand{\haekchen}{{\Large\color{dgreen}\checkmark}}
\newcommand{\attention}{{\scalebox{.75}{\bf\input{svg/attention.pdf_tex}}}\ }
\newcommand{\conclude}{{\color{dred}$\leadsto\;$}}

% Math abbrevs etc...
\renewcommand{\O}[1]{{{\cal O}({#1})}}
\include{tumlogos}

% title
\title{Interprocess Communication in Rust}
\author{Proseminar ``The Rust Programming Language''\\Summer Term 2017}
\institute[TUM]{\vspace{-19em}\tumheader\vspace{18em}}
\date {
    Vasil Sarafov - sarafov@cs.tum.edu\\
    Advisor - M.Sc. Nico Hartmann
}

\newmint{java}{}
\newmint{cpp}{}
\newmint{llvm}{}
\newmint{rust}{}
\newminted{rust}{mathescape,bgcolor=almostnogray}
\newminted{java}{mathescape,bgcolor=almostnogray}
\newminted{cpp}{mathescape,bgcolor=almostnogray}
\newminted{llvm}{mathescape,bgcolor=almostnogray}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  START                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\setbeamertemplate{footline}[frame number]

\maketitle

\section{Outline}

\begin{frame}
\frametitle{Outline}
    \begin{textblock}{12}(0, 1)
        \begin{enumerate}
            \itemsep1.75em
            \item Introduction
            \item Mechanisms for IPC and their translation in Rust
            \item Core Applications
            %\item Useful Crates for IPC
            \item Summary and Discussion
        \end{enumerate}
    \end{textblock} 
\end{frame}


\section{Introduction}

\begin{frame}
\frametitle{1. Introduction}
    \begin{textblock}{4}(0, 1)
        What is IPC? Why bother?\vspace{1em}
    \uncover<2-> {
        \begin{itemize}
            \itemsep1em
            \item Cooperation between \underline{processes}
            \item Data exchange $\rightarrow$ Computation speedup
            \item Design needs (modularity)
        \end{itemize}
    }
    \end{textblock}

    \only<2-> {
    \begin{textblock}{5}(5, 1)
        \includegraphics[width=15em]{svg/ipc_raw.pdf}
    \end{textblock}
    }

    \begin{textblock}{9}(0, 6)
        Why Rust?\vspace{1em}
        \uncover<3-> {
        \begin{itemize}
            \itemsep1em
            \item Because of the Proseminar
            \item Because it's a new, promising alternative to C/C++
            \item Because it enforces safe programming
        \end{itemize}
        }
    \end{textblock} 
\end{frame}



\section{IPC Mechanisms}
\begin{frame}
    \begin{textblock}{9}(1.75, 4)
        \Huge{2. IPC Mechanisms}
    \end{textblock}
\end{frame}

\begin{frame}[fragile]
\frametitle{2.1 Shared Memory}
    \begin{textblock}{10}(0, 1)
        Shared Memory as an IPC mechanism:\vspace{1em}
        \begin{itemize}
            \itemsep1em
            \item (Easiest) and fastest way for IPC
            \item Direct addressing not possible
                \begin{minted}{rust}
let data : &mut DataBlock = unsafe {
    &mut *(0x23387F as *mut DataBlock)
}
                \end{minted}
            \only<2-> {
            \item Memory maps instead
            }
        \end{itemize}
    \end{textblock}
    \only<2-> {
    \begin{textblock}{6}(4, 4.5)
        \includegraphics[width=20em]{svg/memory_mapping_raw.pdf}
    \end{textblock}
    }
\end{frame}

\begin{frame}[fragile]
\frametitle{2.1 Shared Memory (2)}
    \begin{textblock}{10}(0, 1)
        Memory mapping in Rust:\vspace{1em}
        \begin{itemize}
            \itemsep1em
            \item \mintinline{rust}{std::os::MemoryMap} removed in version 0.8
            \item Either raw \mintinline{rust}{libc} FFI bindings or wrapper around it
            \item Solution: \href{https://github.com/danburkert/memmap-rs}{memmap-rs} crate...
        \end{itemize}
    \end{textblock}
\end{frame}

\begin{frame}[fragile]
\frametitle{2.1 Shared Memory (3)}
    \begin{textblock}{10}(0, 1)
        Writing in a memory map:
    \end{textblock}
    \begin{textblock}{10}(0.5, 2.5)
        \begin{minted}{rust}
// handle is an opened read-write file descriptor
let mut map = Mmap::open(&handle, Protection::ReadWrite)
            .expect("Failed to map memory");

unsafe { map.as_mut_slice() }.write(b"TUM").unwrap();
        \end{minted}
    \end{textblock}
\end{frame}

\begin{frame}[fragile]
\frametitle{2.1 Shared Memory (4)}
    \begin{textblock}{10}(0, 1)
        Reading 128 bytes from a memory map:
    \end{textblock}
    \begin{textblock}{10}(1.5, 2)
        \begin{minted}{rust}
// handle is an opened read-only file descriptor
let map = Mmap::open(&handle, Protection::Read)
            .expect("Failed to map memory");

unsafe {
    let mut buf: [u8; 128] = [0; 128];
    let mut mem = map.as_slice();
    mem.read_exact(&mut buf).unwrap();
}
        \end{minted}
    \end{textblock}
\end{frame}

\begin{frame}
\frametitle{2.2 Signals}
    \begin{textblock}{10}(0, 1)
        \only<1->{Signals as an IPC mechanism:\vspace{1em}}
        \only<2->{
        \begin{itemize}
            \itemsep1em
            \item Short asynchronous kernel notifications (\mintinline{bash}{SIGTERM, SIGKILL, ...})
            \item Simple and fast but limited
        \end{itemize}
        }
    \end{textblock}

    \begin{textblock}{10}(0, 5)
        \only<1->{Signals in Rust:\vspace{1em}}
        \only<3->{
        \begin{itemize}
            \itemsep1em
            \item \mintinline{rust}{std::io::signal} removed
                (\href{https://github.com/rust-lang/rfcs/pull/230}{RFC 230} and 
                \href{https://github.com/rust-lang/rust/pull/17673}{PR 17673})
            \item Open proposal - \href{https://github.com/rust-lang/rfcs/issues/1368}{RFC 1368}
            \item Current option - direct \mintinline{rust}{libc} bindings
            \item \href{https://github.com/BurntSushi/chan-signal}{$chan$-$signal$} 
                - synchronous signal handling via channels
        \end{itemize}
        }
    \end{textblock}
\end{frame}

\begin{frame}[fragile]
\frametitle{2.2 Signals (2)}
    \begin{textblock}{5}(2.5, 1)
        \begin{minted}{cpp}
int main() {
    pid_t child = fork();
    if (child) { // in parent
        // do work
        kill(child, SIGKILL);
    } else {
        // do work
    }
    return 0;
}
        \end{minted}
    \end{textblock}
    \only<2-> {
    \begin{textblock}{10}(0, 7)
        \textit{
            ``If pid is -1, \textbf{sig shall be sent to all processes} 
            (...) 
            for which the process has permission to send that signal.''
        }
        \footnote{\href{http://pubs.opengroup.org/onlinepubs/9699919799/functions/kill.html}
        {POSIX specification for kill(2)}}
    \end{textblock}
    }
\end{frame}

\begin{frame}[fragile]
\frametitle{2.2 Signals (3)}
     \begin{textblock}{5}(2, 0.5)
         \mintinline{rust}{pub fn fork() -> Result<ForkResult, i32> { /* */ }}
         \begin{minted}{rust}
pub enum ForkResult {
    Parent { child: pid_t },
    Child
}
         \end{minted}
     \end{textblock}
\end{frame}


\begin{frame}[fragile]
\frametitle{2.2 Signals (3)}
     \begin{textblock}{5}(2, 0.5)
         \mintinline{rust}{pub fn fork() -> Result<ForkResult, i32> { /* */ }}
         \begin{minted}{rust}
pub enum ForkResult {
    Parent { child: pid_t },
    Child
}
         \end{minted}
     \end{textblock}

     \begin{textblock}{6}(2, 3.2)
         \begin{minted}{rust}
fn main() {
    match fork().expect("failed") {
        ForkResult::Parent{child} => {
            // do work
            kill(child, SIGKILL)
            .expect("failed")
        },
        ForkResult::Child => {
            // do work
        }
    }
}
         \end{minted}
     \end{textblock}
\end{frame}

\begin{frame}
\frametitle{2.3 Regular Files}
    \begin{textblock}{10}(0, 1)
        \only<1->{Files as an IPC mechanism:\vspace{1em}}
        \only<2-> {
        \begin{itemize}
            \itemsep1em
            \item Persistent data buffers
            \item Relatively slow for real time communication
        \end{itemize}
        }
    \end{textblock}

    \begin{textblock}{10}(0, 5)
        \only<1->{Files in Rust:\vspace{1em}}
        \only<3-> {
        \begin{itemize}
            \itemsep1em
            \item Full native support with \mintinline{rust}{std::fs::File}
            \item No need to close file descriptors
        \end{itemize}
        }
    \end{textblock}
\end{frame}

\begin{frame}
    \frametitle{2.4.1 Anonymous Pipes}
    \begin{textblock}{10}(0, 1)
        \only<1->{Anonymous Pipes as an IPC mechanism: \vspace{1em}}
        \only<2-> {
        \begin{itemize}
            \itemsep1em
            \item Implicit one-way byte stream communication (same host)
            \item Bound lifetime
            \item No file system allocations
            \item Example: \mintinline{bash}{ls /usr/local/sbin | cat}
        \end{itemize}
        }
    \end{textblock}
    \begin{textblock}{10}(0, 6)
        \only<1->{And in Rust...}
    \end{textblock}
\end{frame}

\begin{frame}[fragile]
    \frametitle{2.4.1 Anonymous Pipes (2)}
    \begin{textblock}{6}(2, 0.5)
        \begin{minted}{bash}
ls /usr/local/sbin | cat
        \end{minted}
    \end{textblock}
\end{frame}

\begin{frame}[fragile]
    \frametitle{2.4.1 Anonymous Pipes (2)}
    \begin{textblock}{6}(2, 0.5)
        \begin{minted}{bash}
ls /usr/local/sbin | cat
        \end{minted}
    \end{textblock}
    \begin{textblock}{6}(2, 2.3)
        \begin{minted}{rust}
pub fn list_sbin() {
    let ls = Command::new("ls")
        .arg("/usr/local/sbin")
        .stdout(Stdio::piped())
        .spawn().unwrap();

    let buf = &mut String::new();
    ls.stdout.unwrap()
        .read_to_string(buf)
        .expect("Pipe failed");
    println!("{}", buf);
}
        \end{minted}
    \end{textblock}
\end{frame}



\begin{frame}
    \frametitle{2.4.2 Named Pipes a.k.a FIFO Files}
    \begin{textblock}{10}(0, 1)
        \only<1->{Named Pipes as an IPC mechanism: \vspace{1em}}
        \only<2-> {
        \begin{itemize}
            \itemsep1em
            \item Explicit one-way byte stream communication (same host)
            \item Unbound lifetime
            \item Namespaced by a file system reference
        \end{itemize}
        }
    \end{textblock}
    \begin{textblock}{10}(0, 5.5)
        \only<1->{Named Pipes in Rust: \vspace{1em}}
        \only<3-> {
        \begin{itemize}
            \itemsep1em
            \item No support in \mintinline{rust}{std}
            \item Creation with \mintinline{rust}{unsafe {libc::mkfifo()}}
            \item Stream manipulation through \mintinline{rust}{std::io}
        \end{itemize}
        }
    \end{textblock}
\end{frame}

\begin{frame}
\frametitle{2.5.1 Unix Domain Sockets}
    \begin{textblock}{10}(0, 1)
        \only<1->{Unix Domain Sockets as an IPC mechanism: \vspace{1em}}
        \only<2-> {
        \begin{itemize}
            \itemsep1em
            \item Bidirectional byte stream communication (same host)
            \item Unbound lifetime
            \item Namespaced by a file system reference
            \item Support for kernel verified data exchange
        \end{itemize}
        }
    \end{textblock}
    \begin{textblock}{10}(0, 6.5)
        \only<1->{And in Rust...}
    \end{textblock}
\end{frame}


\begin{frame}
\frametitle{2.5.1 Unix Domain Sockets (2)}
    \begin{textblock}{10}(0, 1)
        \only<1->{Unix Domain Sockets in Rust: \vspace{1em}}
        \begin{itemize}
            \itemsep1.25em
            \only<1-> {
            \item Serverside: 
                \begin{itemize}
                    \itemsep0.75em
                    \item \mintinline{rust}{std::os::unix::net::UnixListener::bind(&handle)}
                    \item \mintinline{rust}{UnixListener.accept()}
                \end{itemize}
            }
            \only<2-> {
            \item Clientside: 
                \begin{itemize}
                    \itemsep0.75em
                    \item \mintinline{rust}{std::os::unix::net::UnixStream::new(&handle)}
                \end{itemize}
            }
            \only<3-> {
            \item Stream manipulation with:
                \begin{itemize}
                    \itemsep0.75em
                    \item \mintinline{rust}{std::io::Read}
                    \item \mintinline{rust}{std::io::Write}
                \end{itemize}
            }
        \end{itemize}
    \end{textblock}
\end{frame}

\begin{frame}
    \frametitle{2.5.2 Network Sockets}
    \begin{textblock}{10}(0, 1)
        \only<1->{Network Sockets as an IPC mechanism: \vspace{1em}}
        \only<2-> {
        \begin{itemize}
            \itemsep1em
            \item Same as domain sockets but for remote host connection
        \end{itemize}
        }
    \end{textblock}
    \begin{textblock}{10}(0, 3.5)
        \only<1->{Network Sockets in Rust: \vspace{1em}}
        \only<3-> {
        \begin{itemize}
            \itemsep1em
            \item \mintinline{rust}{std::net} for TCP/UDP communication
            \item IPv4 and IPv6 supported
            \item Stream manipulation as in domain sockets
        \end{itemize}
        }
    \end{textblock}
\end{frame}

\begin{frame}[fragile]
\frametitle{IPC with Rust at a glance}
    \begin{textblock}{10}(0, 1)
        \begin{center}
        \begin{tabular}{| l | c | c |}
            \hline
            Mechanism & \mintinline{rust}{std} & crate \\[0.4em] \hline
            Memory Map & \no & \yes \\[0.7em]
            Signals & \no & \yes \\[0.7em]
            Regular Files & \yes & - \\[0.7em]
            Anonymous Pipes & \yes & - \\[0.7em]
            Named Pipes & \no & \yes\footnote{Only unsafe \mintinline{rust}{libc} bindings} \\[0.7em]
            Unix Domain Sockets & \yes & - \\[0.7em]
            Network Sockets & \yes & - \\
            \hline
        \end{tabular}
        \end{center}
    \end{textblock}
\end{frame}

\section{Applications}
\begin{frame}
    \begin{textblock}{9}(2.5, 4)
        \Huge{3. Applications}
    \end{textblock}
\end{frame}

\begin{frame}[fragile]
\frametitle{3.1 Remote Procedure Calls}
    \begin{textblock}{10}(0, 1)
        What is a RPC?
    \end{textblock}

    \begin{textblock}{6}(2, 2)
        \begin{minted}{rust}
let x = foo::bar(12, 13)
// foo::bar exists on a remote host
// x is calculated remotely
        \end{minted}
    \end{textblock}

    \begin{textblock}{10}(0, 5)
        What about Rust?
        \only<2-> {
        \begin{itemize}
            \itemsep1em
            \item Implementation of the \href{https://capnproto.org/}{\textit{\underline{cap'n proto RPC protocol}}}
            \item \href{https://github.com/dwrensha/capnproto-rust}{$capnproto$-$rust$}
                for the type system (serialization)
            \item \href{https://github.com/dwrensha/capnp-rpc-rust}{$capnp$-$rpc$-$rust$}
                for the RPC protocol
        \end{itemize}
        }
    \end{textblock} 
\end{frame}

\begin{frame}
\frametitle{3.2 Microkernels}
    \begin{textblock}{9}(0, 1)
        What is a $\mu$-kernel?
    \end{textblock}

    \only<2-> {
    \begin{textblock}{9}(1.3, 1.5)
        \includegraphics[width=23em]{svg/microkernel_raw.pdf}
    \end{textblock}
    }

    \begin{textblock}{9}(0, 8.5)
        What about Rust?
    \end{textblock} 
\end{frame}

\begin{frame}
    \frametitle{3.2 Microkernels (2)}
    
    \begin{textblock}{10}(2, 0.5)
        \href{https://github.com/redox-os/redox/blob/master/README.md}{\includegraphics[width=20em]{images/redox-os-logo-full.png}}\\
        \hspace*{8.5em}\footnotesize{The official RedoxOS logo}
    \end{textblock}

    \begin{textblock}{10}(0, 5)
        \begin{itemize}
            \itemsep1.25em
            \item A full-featured operating system written in Rust
            \item Microkernel design
            \item Currently support only for x86\_64
            \item MIT licensed
        \end{itemize}
    \end{textblock}
\end{frame}


\section{Summary}

\begin{frame}
\frametitle{5. Summary}
    \begin{textblock}{9}(0, 1)
        Rust has:
    \end{textblock}
    \begin{textblock}{9}(0, 2)
        \begin{itemize}
            \itemsep1.35em
            \item strong type system + compile time checks $\rightarrow$ safer code
            \item \underline{no} ``batteries included'' for IPC
            \item highly skilled community $\rightarrow$ plenty of open source crates
            \item been successfully utilized in IPC systems
        \end{itemize}
    \end{textblock} 
\end{frame}

%TODO: Improve the bibliography
\section{Resources}
\begin{frame}
\frametitle{Resources}
\tiny
\nocite{*}
  \bibliographystyle{abbrv}
  \bibliography{literatur}
\end{frame}


\begin{frame}
    \begin{textblock}{9}(0.5, 2)
        \Huge{Thank you for your attention}
    \end{textblock}
    \begin{textblock}{9}(2.5, 6)
        Slides and source code can be found at\\
        \url{http://home.in.tum.de/~sarafov}
    \end{textblock} 
\end{frame}

\begin{frame}
    \begin{textblock}{9}(1.5, 4)
        \Huge{Questions/Discussion}
    \end{textblock}
\end{frame}

\begin{frame}
    \begin{textblock}{9}(2.5, 4)
        \Huge{Backup Slides}
    \end{textblock}
\end{frame}


\begin{frame}
\frametitle{3.3 Platform Communication Stack}
    \begin{textblock}{9}(0, 1)
        What is a platform communication stack?
    \end{textblock}

    \only<2-> {
    \begin{textblock}{9}(0, 2)
        \begin{itemize}
            \itemsep1.5em
            \item Standardized way of IPC in a modularized platform
            \item Intuitive analogy: ``Higher abstraction of the $\mu$-kernel idea''
            \item Example: desktop server
        \end{itemize}
    \end{textblock}
    }

    \begin{textblock}{9}(0, 6)
        What about Rust?
    \end{textblock} 
\end{frame}

\begin{frame}
    \frametitle{3.3 Platform Communication Stack (2)}
    \begin{textblock}{10}(0.2, 1)
        \includegraphics[width=33em]{images/orbital-de-1.png}\\
        \hspace*{8.5em}\footnotesize{RedoxOS desktop environment - Orbital}
    \end{textblock}
\end{frame}


\section{Useful Crates}
\begin{frame}
\frametitle{4. Useful Crates}
    \begin{textblock}{9}(0, 1)
        \begin{itemize}
            \itemsep1.25em
            \item \href{https://github.com/rust-lang/libc}{$libc$} 
                - raw and unsafe OS-specific C bindings 
            \item \href{https://github.com/nix-rust/nix}{$nix$} 
                - safe C bindings for Unix-like operating systems
            \item \href{https://github.com/danburkert/memmap-rs}{$memmap$-$rs$} 
                - cross platform memory mapping
            \item \href{https://github.com/BurntSushi/chan-signal}{$chan$-$signal$} 
                - synchronous signal handling via channels
        \end{itemize}
    \end{textblock} 
    \begin{textblock}{9}(3.5, 5.5)
        \uncover<1->{
            \href{http://crates.io}{\includegraphics[width=8.5em]{images/cargo-logo.png}}\\
            \footnotesize{The official crates.io logo}
        }
    \end{textblock}
\end{frame}

\end{document}
