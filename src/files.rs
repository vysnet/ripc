use rand;
use regex::Regex;
use std::fs::File;
use std::io::Read;
use std::io::Write;

static LOG_FILE: &'static str = "/tmp/ripc-printer.log";

pub struct Agent { name: String }
pub struct Printer { name: String }

impl Agent {
    pub fn new(name: String) -> Agent {
        Agent { name: name }
    }

    pub fn start(self) {
        println!("Agent {} started", self.name);

        let buf = &mut String::new();
        let _ = File::open(LOG_FILE)
            .expect("Could not open the log file")
            .read_to_string(buf)
            .expect("Could not read from the log file");

        let lines: Vec<&str> = buf.split("\n").collect();

        let info_regex = Regex::new(r"^.*INFO.*$").unwrap();
        let err_regex = Regex::new(r"^.*ERR.*$").unwrap();
        let (infos, errors) = (&mut 0, &mut 0);

        //Experiment with borrowing inside a closure
        let check = move |text: &str, reg: &Regex, counter: &mut i32| {
            if reg.is_match(text) {
                *counter += 1;
            }
        };

        for line in &lines {
            check(&line, &info_regex, infos);
            check(&line, &err_regex, errors);
        }

        println!("Log summary:\nInfos total: {}\nErrors total: {}", infos, errors);
    }
}


impl Printer {
    pub fn new(name: String) -> Printer {
        Printer { name: name }
    }

    pub fn start(self) {
        println!("Printer {} started", self.name);

        let mut f = File::create(LOG_FILE).unwrap();

        println!("Printing...");
        let bound = rand::random::<u8>();
        for _ in 0..bound {
            if rand::random() {
                f.write_all("ERR\n".as_bytes()).unwrap();
            } else {
                f.write_all("INFO\n".as_bytes()).unwrap();
            }
        }

        println!("Done!");
    }
}


