# Interprocess Communication in Rust

## Project Layout
Vasil Sarafov - sarafov@cs.tum.edu

-------------------------------------------------------

### 1. Introduction
A short introduction to the basic terms and principles 
that are essential for understanding this paper.
- What does actually IPC mean? How is it done?
- Why do we need to care about interprocess communication (IPC)?
- Why was Rust chosen as an implementation language?
- Explanation why `libc`/`rlibc` plays a huge role for our discussion.
(libc is the linker between rust and the host operating system).
- Make sure to let the readers know what the targeted environment  
looks like (Unix operating system, no bare metal).

-------------------------------------------------------

### 2. Techniques for Interprocess Communication

**Note** To each presented technique there is a code demo. (Check the `ripc` folder).
Some of the demo code can be simplified so that it can be put in the paper.
A more detailed description for some code segments can be then made in the according section.

2.1 Shared Memory
- The easiest and fastest way for two or more processes to communicate with each other 
is to share memory. 
- No action is needed by the kernel to orchestrate the communication because the processes
are writing to the memory as if it were normal data.
- Of course direct addressing of the desired memory blocks is not possible 
since every process is assigned a virtual address space that is managed by the kernel.
- That's why the shared memory has to be allocated and then explcitly mapped 
to the virtual address space of each process. The allocated memory segment can be anonymous,
which can be then shared between processes with a common ancestry, or a memory mapped file.
In the latter case the file handle is only used for identifying purposes, 
no actual content is being written to the file system 
(it must be assured that the kernel has mapped the same memory region to all participating processes).
- A good theoretical explanation about the differences between shared memory and memory mapped files
can be found in [this paper](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n2044.html#Introduction).
Spoiler alert - there is no difference for the POSIX compliant operating systems.
- In [version 0.8](https://github.com/rust-lang/rust/blob/master/RELEASES.md#version-08-2013-09-26) 
of rust cross platform support for memory mapping via `std::os::MemoryMap` was introduced. 
Later in version 1.0 it was deprecated in favour of using raw `libc` bindings 
[citation needed, check git logs again](https://users.rust-lang.org/t/memory-mapped-io/693/2).
- Luckily there are community crates which tackle this issue. 
The most popular of which is the cross platform `memmap` (see list of crates below).

2.2 Signals
- Signals are one of the most simple forms of IPC. They are short asynchronous notifications
that are sent to a process (or a specific thread inside a process) when an event has occurred.
- Currently there is _no native support_ in the rust `std` library for signal handling. 
However there is an open proposal about it - [RFC 1368](https://github.com/rust-lang/rfcs/issues/1368).
- In the earlier days of rust there was support for signal handling using the language's runtime environment.
Rust has removed its own runtime environment into an external crate as described in
[RFC 230](https://github.com/rust-lang/rfcs/pull/230) and shifted to a native threading model.
Because of this the `std::io::signal` module dropped from the standard library - 
[PR 17673](https://github.com/rust-lang/rust/pull/17673).
- Therefore the only solution to operate with signals with rust at the time of writing is through FFI bindings.
- Handling signals can be implemeted safely by using the [chan-signal crate](https://github.com/BurntSushi/chan-signal)
using a non-standard channel-thread-blocking approach. Nevertheless this might not be suitable for all use cases.
Of course it's possible to directly use `libc` instead and create an asychronous signal handler. 
However it's unsafe.
- Sending signals can be done using a safe `libc` binding using the `rust-nix/nix` crate.
- Interesting example, showing the importance of safety when dealing with signals:
[signal killing rampage](http://kamalmarhubi.com/blog/2016/04/13/rust-nix-easier-unix-systems-programming-3/).

2.3 Regular Files
- A regular file can be used as a persistent message buffer inside an interprocess communication pool.
- This can be however slow (content must be written to the file system).
- Full native support for file operations (reading, writing, appending, seeking, ...) 
is provided by `std::fs::File` which implements
the `std::io::Read` and `std::io::Write` traits (and more).
- There is no need to bother closing file descriptors. 
All closing calls will be automatically inserted by the compiler at compile time.

2.4 Pipes

2.4.1 Anonymous pipes 
- Anonymous pipes provide implicit one-way bytestream communication between processes.
In most cases those processes are a father-child pair, 
meaning that the pipe users must have a common ancestry.
- The lifetime of an anonymous pipeline is limited to the lifetime of the processes using it
(It's acreated when it's needed and destroyed by the kernel when it's no longer needed).
- It does not allocate any resources on the file system.
- A file descriptor, pointing to an anynomous pipe can be created using `std::process::Stdio::piped()`. 
It can be then used to redirect an arbitrary stream: `stdio`, `stdout`, `stderr`.

2.4.2 Named pipes (FIFO files on Unix)
- Named pipes provide explicit bytestream communication between 
2 or more processes on the same host machine.
- They must be explictly created and destroyed (difference to anonymous pipes).
- A named pipe is referenced by the file system but does not 
allocate any resources on the hard drive as the kernel passes the messages directly to the pipe
without writing it on the file system (difference with regular files, hence faster).
- Currently the only way to create a named pipe 
in rust is to use the unsafe `libc::mkfifo()` binding.
- Reading and writing is done via the normal `std::io` API with the only difference that the pipe
must be opened in the correct mode (read or write only on each side).

2.5 Sockets (Communication endpoints)

2.5.1 Unix domain sockets
- Unix domain sockets allow a bidirectional communication  (difference to pipes)
between multiple processes on the same host machine by using the file system as their namespace.
Moreover unix domain sockets are managed by the kernel and therefore
a pool of processes is able to exchange kernel verified data such as credentials, UIDs, GIDs, etc...
- Rust comes with native support for unix domain socket communication.
- Using `std::os::unix::net::UnixListener::bind()` a new listener (server)
bound to the specified socket can be created. 
New connections can be then accepted with the `UnixListener.accept()` method.
- Using the `std::os::unix::net::UnixStream` the client can connect to the server.
- Once a connection is accepted (from the server side) or established (from the client side),
information between the participants in the communication pool can be exchanged
by manipulating the underlying stream which implements the 
`std::io::Read` and `std::io::Write` traits.

2.5.2 Network sockets
- Similary to Unix domain sockets, rust comes with support out of the box for network sockets.
- The module [`std::net`](https://doc.rust-lang.org/nightly/std/net/index.html) 
contains networking primitives for TCP/UDP communication.
- Both IPv4 and IPv6 are supported.
- The API for manipulating a TCP/UDP communication is exactly the same as in the unix domain sockets.
- Additionally there are some utility structures such as 
[`lookup_host`](https://doc.rust-lang.org/nightly/std/net/fn.lookup_host.html)
for resolving the socket address from a hostname.

------------------------------------------------------------------------------

### 3. Applications
3.1 RPCs - Remote Procedure Calls
- RPCs are built on top of a system, which utilizes multiple IPC mechanisms.
- It is an asbtraction of the procedure-call mechanism for use between systems with a network connection.
- RPCs allow a client to invoke a procedure on a remote host as it would invoke a procedure locally.
- This is achieved by interacting with a _stub_ which then delegates the needed data 
(arguments, control units, etc) in a serialized form to the remote host.
- Usage example: distributed file system
- [cap'n proto](https://capnproto.org/)
- [`capnproto-rust`](https://github.com/dwrensha/capnproto-rust) and 
[`capnp-rpc-rust`](https://github.com/dwrensha/capnp-rpc-rust).

3.2 Microkernels
- Microkernel design is an alternative way of structuring an operating system kernel, 
compared to the standard monolithic approach.
- This method structures the operating system by removing all nonessential
components from the kernel, and implementing them as system- and user-space programs.
- The result is a notedly more compact kernel, hence the safety-critical surface is smaller.
- Microkernels are possible because they provide an advanced communication facility 
for the user-space processes to use.
- [RedoxOS](https://www.redox-os.org/)

3.3 Platform Communication Stack
- A platform communication stack provides a standardized way for processes of a given platform 
(on the same host) to communicate with each other by utilizing IPC mechanisms.
- By doing so, one makes the platform open for external developers (extending it is a lot easier, since
the coorperation with the other platform units is possible).
- One can think of it as a higher abstraction of the microkernel approach 
(the microkernel is a userspace process, which manages higher resources).
- A typical example is a desktop server, where each program that needs graphical resources can
connect to the server (that is, it acts as a client). All clients can then act as peers of each other
and share resources if needed.
- The desktop environment of RedoxOS - [Orbital DE](https://github.com/redox-os/orbutils) -
provides a trivial implementation of a 
[platform communication stack](https://github.com/redox-os/orbutils/blob/master/src/launcher/main.rs).

------------------------------------------------------------------------------

### 4. Important Crates and Libraries
4.1 [`libc`](https://github.com/rust-lang/libc) - 
Raw (unsafe, direct and without a unified public API) rust bindings for easier work with C code 
(system calls, third party utilies, etc) on each platform that is supported by Rust.
Detailed description in [RFC 1291](https://github.com/rust-lang/rfcs/blob/master/text/1291-promote-libc.md).

4.2 [`nix`](https://github.com/nix-rust/nix) - 
Friendly rust bindings to nix platform APIs (Linux, Darwin, BSD, etc).
It wraps many raw system APIs exposed by `libc` into safe abstractions that 
follow the rust paradigms for safe programming.

4.3. [`memmap-rs`](https://github.com/danburkert/memmap-rs) - 
Cross platform memory mappings (anonymous memory blocks, files).

4.4 [`chan-signal`](https://github.com/BurntSushi/chan-signal) -
Synchronous (thread-blocking) signal handling with channels.

4.5 [`capnproto-rust`](https://github.com/dwrensha/capnproto-rust) and 
[`capnp-rpc-rust`](https://github.com/dwrensha/capnp-rpc-rust) - 
Rust implementations of the [cap'n proto](https://capnproto.org/) 
type system and remote prodecure call protocol.

Note: More crates will come

------------------------------------------------------------------------------

### 5. Summary
- Rust's strong type system and compile time checks transform a lot of runtime errors into compile time errors,
which can prevent critical faults as seen in (2.2).
- As seen in the examples, rust does not come with "batteries included" when it comes to IPC.
- Core concepts such as signal handling and memory mapping lack support in the standard library at the moment.
- However the community around the language is highly skilled and although not very big, 
it has already managed to fill the gaps in the standard library with additional open source crates.
- There are big open source projects (as seen in 3), which utilize IPC as their core concept

------------------------------------------------------------------------------

### Notes and Impressions (additional)
- It's really cumbersome and time consuming to work with the documentation 
compared to the traditional man pages 
(need to exit the terminal, navigate inside the browser with the mouse, etc)
- Once you know the syntax it's a joy to write. 
However until the API is known it can be a really painful ride.
- The majority of rust's community is highly skilled.
Maybe because the learning curve is higher than in other programming languages
(javascript, lol).
- Somehow there is a strange feeling that rust's API is not stable and
the running code you are writing now could be potentially no longer
running in a couple of months when the new rust version is out.
(Probably it's a placebo effect or just a wrong reasoning, conlcuded when reading issues/code
for the old versions - the language now has nothing to do with the language 20 months ago).
- Some of the compiler messages are ridiculous and can cost you a lot of debugging time.
- A lot of already existing programming concepts and structures are given new names
which can be confusing for newcomers (arrays vs slices, wtf).
- The package system is not intuitive. It costs a big time overhead.
- Why would you want to monkeypatch a systems language? Strong influence from ruby...
This feature is causing more pain than joy because every trait, 
whose implementation is needed, must be explictly imported.
- There are already quite a few community crates out there!

------------------------------------------------------------------------------
