BIN:=ripc

.PHONY: all clean

all:
	cargo build
	cp ./target/debug/$(BIN) $(BIN)

clean:
	rm -f $(BIN)
