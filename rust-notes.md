# Rust Notes

## Table of Contents
- [The Book](https://doc.rust-lang.org/book/)
- [General](#general)
- [Generics and Traits](#generics-and-traits)
- [Ownership System](#ownership-system)
- [Mutability](#mutability)
- [Strings](#strings)
- [Closures](#closures)
- [Miscellaneous](#miscellaneous)

## General
- Rust is very similar to OCaml and is a functional language (not pure though).
- Everything in rust is an expression. Everything evaluates to a given value.
- Statements are expressions which are terminated by _;_ and their return value is _()_.
- The `enum` in rust is equivalent to `type` in OCaml.
- The `type` in rust is equivalent to `typedef` in C++ (it declares a type alias).
- Rust is not an OOP language. It uses the notion of `struct` with the _Builder Design Pattern_ in mind.
A `struct` describes the entity's attributes and a `impl` block provides its functions/methods.
- `self` in rust is the same as in Python, including its living cycle inside an entity 
(it is passed as the first argument to a function/method).
- By default everything is private to the source file. A entity 
(module, struct and its attributes, function) can be made public to external users with `pub`.
- `use` in rust is very much like `import` in Python.
- `const` in rust is a mexture between `const` and `define` in C++ when used for defining constants. 
A single constant definition can result in more than one copy of the value in memory _(compiler inlining)_.
- `static` in rust declares a global, possibly mutable, constant that is not inlined by the compiler.
It lives for the entire lifetime of a program. Performance and safety wise it's 
better to use `const` instead of `static` 
[source](https://doc.rust-lang.org/book/const-and-static.html#which-construct-should-i-use).

## Traits and Generics
- Generics in rust are very similar to type templates in C++. 
Each _generic type_ is inferred at compile time and the _generic entity_ is compiled to a 
new copy with the appropriate data type. This process is called _static dispatch_.
- Traits in rust are like interfaces in Java and are used as contracts.
Built in or cargo defined data types can be monkey patched by user defined traits.
User defined data types can be monkey patched by imported built in or cargo defined traits.
- The code bloat caused by the compilation of generic entities can be prevented by using _dynamic dispatch_.
This is achieved by explicitly casting an object to a trait object - `func(&x as &Trait)` - or 
implicitly coercing it to a trait object - `func(&x)`, given that `fn func(x: &Trait) {}`.
Dynamic dispatch sacrifices execution speed for a tinier binary footprint 
(because the resolution of the virtual function calls is done at runtime, which is more expensive).
- Only "object-safe" traits can take advantage of _dynamic dispatch_. As a rule of thumb: 
except in special circumstances, if the trait’s method uses `Self`, it is not object-safe.

## Ownership System
- Each entity (variable, data structure, etc..) can have only one owner
_(this enforces automatic memory deallocation without the need of garbage collector)_.
- Heuristic: the compiler inserts a _deallocation statement_ for each entity at the end of the lifetime of its last owner.
- Passing an entity *by value* is *transferring the ownership* to the callee.
- Passing an entity *by reference* is *borrowing* the ownership. That means
that the callee returns the ownership to the caller implicitly. 
The same can be achieved when only passing by value but it's more cumbersome 
since returning of the ownership must be typed explictly.
- By default borrowing is immutable. It can be enforced as mutable with `&mut T` for example.
- Each scope can have only one of the following types of borrows _(this prevents data races)_:
    - one or more immutable references `&T`
    - exactly one mutable reference `&mut T`
- Lifetimes ensure that we don't access resources that are not present anymore
_(this prevents dangling pointers)_. For example:
    - a struct is outliving a member value
    - a function is returning a reference to a resource that is being outlived by the saver

## Mutability
- The real definition of 'immutability' is: is this safe to have two pointers to?
The definition is tightly coupled to the internal behaviour of the resource (data structure).
- Mutability can only be enforced with `let mut` or `&mut`
- `struct` cannot have mixed (in the sense of mutability) attributes. 
The mutability of the struct entity is controlled by the binding `let foo` or `let mut foo`.
- Mutability is a property of the binding, not of the structure itself.

## Strings
- There two types of strings in rust:
    - Allocated statically at compile time (string slices): `let s: &'static str = "text"`
    - Living on the heap, growing at run time: `let s: String = "text".to_string()`
- All strings are guaranteed to be a valid encoding of UTF-8 sequences.
- Strings are not NUL-terminated and can contain NUL bytes.
- They cannot be indexed with _[]_ because of their UTF-8 nature 
(each character can consist of different byte size). 
That's why a full iteration trough the string is always needed when a special character is desired.
- Concatenation is done with `String + &str`:
```
let hello: String = "Hello ".to_string();
let world: String = "world!".to_string();
let word: String = hello + &world;
```

## Closures
- Chapter in the book: [here](https://doc.rust-lang.org/book/closures.html)
- The general syntax for closures is: `let f = |arg: type| -> ret_type { expression }`
- Closures borrow the outside entities they use by default.
- Closers can force borrowing a copy of the outside entities instead with `move`.
`move` gives the closure its own stackframe.
- Under the hood rust implements closures as trait objects.

## Pointers
- Safe pointers that follow the rules of the [ownership system](#ownership-system), 
are checked at compile time and much more:
    - `&` and `&mut` are safe references.
    - `std::boxed::Box<T>` is a safe pointer for heap allocation. `Box::new(x)` 
allocates memory on the heap and then places `x` into it.
- Unsafe (raw) pointers bypass the [ownership system](#ownership-system), meaning that they can be dangling,
are not automatically cleaned-up and etc. Their dereferencing must occur in a `unsafe` block
[source](https://doc.rust-lang.org/book/raw-pointers.html):
    - `*const T` - raw pointer that will not change the value it points to; 
however it could be NULL and leak memory.
    - `*mut T` - raw pointer that can change the value it points to.

## Miscellaneous
- The `Option<T>` type with values `Some(T)` and `None` is inteded to be used to return 
a given value if it's present, and nothing if the value is missing.
- The `Result<T, E>` type with values `Ok(T)` and `Err(E)` is intended to be used to return the result of a computation, 
and to have the ability to return an error if it didn’t work out.
- The `Drop` trait is like a destructure in C++. It's `drop()` method will be called before the entity is freed.

