#[macro_use]
extern crate clap;
extern crate ripc;

use ripc::pipes;
use ripc::files;
use ripc::memory;
use ripc::signals;
use ripc::sockets;
use std::hash::Hash;
use std::hash::Hasher;

fn main() {
    //Initialize the CLI
    let configs = load_yaml!("cli.yml");
    let app = clap::App::from_yaml(configs).get_matches();

    //Generate a unique timestamp for the current program execution
    let state = &mut std::collections::hash_map::DefaultHasher::new();
    let _ = std::time::SystemTime::now()
        .duration_since(std::time::UNIX_EPOCH)
        .expect("Failed to generate unix epoch timestamp")
        .hash(state);
    let timestamp = state.finish() % 10000;

    //Parse the CLI and load the appropriate demo submodule
    match app.subcommand() {
        ("signals", Some(ref args)) => {
            if args.is_present("worker") {
                signals::Worker::new(timestamp.to_string()).start();
            } else if args.is_present("boss") {
                let err: &str = "Could not parse the worker id";
                let boss = signals::Boss::new(timestamp.to_string());
                let wid = i32::from_str_radix(&args.value_of("wid").expect(err), 10).expect(err);
                boss.send_permission(wid);
            }
        },

        ("files", Some(ref args)) => {
            if args.is_present("agent") {
                files::Agent::new(timestamp.to_string()).start();
            } else if args.is_present("printer") {
                files::Printer::new(timestamp.to_string()).start();
            }
        },

        ("pipes", Some(ref args)) => {
            if args.is_present("anonymous") {
                pipes::list_sbin();
            } else if args.is_present("producer")  {
                pipes::named_producer();
            } else if args.is_present("consumer") {
                pipes::named_consumer();
            }
        },

        ("sockets", Some(ref args)) => {
            if args.is_present("unix_server") {
                sockets::unix_server();
            } else if args.is_present("unix_client") {
                sockets::unix_client();
            } else if args.is_present("echo_server") {
                sockets::echo_server();
            } else if args.is_present("echo_client") {
                sockets::echo_client();
            }
        },

        ("memory", Some(ref args)) => {
            if args.is_present("producer") {
                memory::producer();
            } else if args.is_present("consumer") {
                memory::consumer();
            }
        },

        _ => {},
    }
}
