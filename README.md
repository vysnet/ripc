# Interprocess Communication with Rust

## Overview
This repository contains the work I conducted during the seminar ["The Rust Programming Language"](http://www2.in.tum.de/hp/Main?nid=347), offered by the chair for Programming Languages and Descriptive Structures in Summer Term 2017 at TUM. I examined how interprocess communication can be implemented with [Rust](https://www.rust-lang.org/en-US/).

See the [downloads](#downloads) section below for direct access to the materials.

## Downloads

| Asset        | link        |
| :----------- | :---------: |
| Talk slides  | [(pdf)](https://gitlab.com/v45k0/ripc/raw/master/talk/talk.pdf)   |
| Paper        | [(pdf)](https://gitlab.com/v45k0/ripc/raw/master/paper/seminarpaper.pdf)   |
| CLI demo app | [(compile)](#build-from-source) |

## Build from source
- To build the CLI demo application: execute `make` in the root directory of the project (requires rustc 1.17 or newer)
- To build the paper from source (requires latexmk), execute `cd paper && make` from the root directory of the project.
- To build the talk slides from source (requires latexmk), execute `cd talk && make` from the root directory of the project
